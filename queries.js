const Pool = require('pg').Pool
const pool = new Pool({
  user: 'me',
  host: 'localhost',
  database: 'rentiavto',
  password: 'password',
  port: 5432,
})

const getUsers = (request, response) => {
    pool.query('SELECT * FROM rentiavto.users ORDER BY id ASC', (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).json(results.rows)
    })
}

const login = async (request, response) => {
  const {email,password} = request.body

  const {rows} =  await pool.query('SELECT * FROM rentiavto.users WHERE email = $1', [email])
  if(rows.length == 0){
    response.status(404).json({error:"Email does not exist"}).end()
    return
  }

  if(rows[0].password != password){
    response.status(404).json({error:"Password is incorrect"}).end()
    return
  }

  response.status(200).json({userId: rows[0].id}).end()
  return
}

const register = async (request, response) => {
  const {name,surname,email,password} = request.body

  let {rows} =  await pool.query('SELECT * FROM rentiavto.users WHERE email = $1', [email])
  if(rows.length != 0){
     response.status(404).json({error:"Email already exist"}).end()
     return
  }

  let id = await pool.query(`INSERT INTO rentiavto.users (name,surname,email,password) 
                                      VALUES ($1, $2, $3, $4) RETURNING id`, [name,surname,email,password])
  response.status(200).json({userId: id}).end()
  return
}


const getUserById = (request, response) => {
    const id = parseInt(request.params.id)
  
    pool.query('SELECT * FROM rentiavto.users WHERE id = $1', [id], (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).json(results.rows)
    })
}

const updateUser = (request, response) => {
    const id = parseInt(request.params.id)
    const { name, email } = request.body
  
    pool.query(
      'UPDATE rentiavto.users SET name = $1, email = $2 WHERE id = $3',
      [name, email, id],
      (error, results) => {
        if (error) {
          throw error
        }
        response.status(200).send(`User modified with ID: ${id}`)
      }
    )
}

const deleteUser = (request, response) => {
    const id = parseInt(request.params.id)
  
    pool.query('DELETE FROM rentiavto.users WHERE id = $1', [id], (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).send(`User deleted with ID: ${id}`)
    })
}


module.exports = {
    getUsers,
    getUserById,
    login,
    register,
    updateUser,
    deleteUser,
}